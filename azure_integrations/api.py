from __future__ import unicode_literals
import frappe, base64, hashlib, hmac, json
from frappe import _
from datetime import datetime
from frappe.utils import cint, date_diff, datetime, get_datetime, today

@frappe.whitelist(allow_guest=True)
def api_data(*args):
    """
        Sales Order Cancelation and status change
    """
    test_data = json.loads(frappe.request.data)
    try:
        api_response = frappe.new_doc("Api Logs")
        api_response.title = "Data Inserted"
        api_response.created_datetime = today()
        api_response.payload = str(test_data)
        api_response.api_status = "200"
        api_response.response = "User Data API call Successful"
        api_response.flags.ignore_permissions = True
        api_response.save()
        frappe.db.commit()
        return {"message":"User Data API call Successful"}

    except Exception as e:
        api_response.title = "Data Called with Error Response"
        api_response.created_datetime = today()
        api_response.payload = str(test_data)
        api_response.api_status = "500"
        api_response.response = str(e)
        api_response.flags.ignore_permissions = True
        api_response.save()
        frappe.db.commit()
        return {"message":e}