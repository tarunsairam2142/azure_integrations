from frappe import _

def get_data():
	return [
		{
			"module_name": "Azure Integrations",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Azure Integrations")
		}
	]
