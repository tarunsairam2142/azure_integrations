from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in azure_integrations/__init__.py
from azure_integrations import __version__ as version

setup(
	name="azure_integrations",
	version=version,
	description="integrations with azutre",
	author="tarun",
	author_email="tarunsairam2142@gmail.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
